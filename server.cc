/*
 * Project:		CS 3505 Spreadsheet Server
 * Team: 		Sheetweavers
 * Date:		4/8/2014
 * Citation:	http://codebase.eu/source/code-cplusplus/multithreaded-socket-server/download/server.cpp
 */
 
#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <pthread.h>
#include <sstream>
#include <cstdio>
#include <cstdlib>

using namespace std;

/*
 * Lock Object for Synchronization
 */
pthread_mutex_t lock_state = PTHREAD_MUTEX_INITIALIZER;

/*
 * Send repsonse to client.
 * @param sd - socket descriptor
 * @param data - message in string format
 */
int send_message(int sd, string data)
{
    int ret;

    ret = send(sd, data.c_str(), strlen(data.c_str()),0);
    return 0;
}

/*
 * Connection Maintenance - Send / Receive etc.
 * @param void * socket descriptor
 */
 void * respond(void * sd) 
 {
 
	// Recover Socket
	int client_socket;
	client_socket = *((int*)&sd);
	
	// Allocate Buffer
	char buffer[256];
	
	while(1) {
	
		// Zero buffer
		bzero(buffer,256);
		
		// Get Bytes
		int numBytes;
		numBytes = read(client_socket,buffer,255);
		
		if (numBytes <= 0)
        {
            cout << "Client Disconnected." << endl ;
            close(client_socket);
            pthread_exit(0);
        }
		
		// Display message
		printf("Client: %d\nMessage: %s",client_socket,buffer);
		
		// Lock
		pthread_mutex_lock(&lock_state);
		
		// Send Response
		send_message(client_socket,"Received Request!\n");
		
		// Unlock
		pthread_mutex_unlock(&lock_state);
	}
 }
 
/*
 * Server Start and Initialization
 */
int main(int argc, char *argv[])
{

	// Declarations
	int server_socket;
	int port;
	socklen_t clilen;
	int client_socket;
	pthread_t client_thread;
	struct sockaddr_in serv_addr, cli_addr;
	
	// Determine port #
	if (argc < 2) {
		fprintf(stderr,"Please provide port number as CL argument.");
		exit(1);
	}
	
	// Allocate server_socket resources
	server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket < 0) {
		cout << "Unable to allocate server_socket for server." << endl;
		exit(1);
	}
	
	// Server and Client Addresses
    bzero((char *) &serv_addr, sizeof(serv_addr));

    // Port Number
	port = atoi(argv[1]);
	
    // Byte Order
    serv_addr.sin_family = AF_INET;  

    // Fill with IP Address
    serv_addr.sin_addr.s_addr = INADDR_ANY;  

    // Port Number conversion
    serv_addr.sin_port = htons(port);

	// Bind server_socket to port number and address
    if (bind(server_socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        cout << "ERROR on binding" << endl;
		exit(1);
	}
	
	// Listen and place in queue for connection completion
    listen(server_socket,5);
	clilen = sizeof(cli_addr);
		
	while(1) {
	
		// Socket Thread Descriptor
		int client_socket;
	
		// Accept
		client_socket = accept(server_socket, (struct sockaddr *) &cli_addr, &clilen);
		if (client_socket < 0) {
			cout << "ERROR on accept" << endl;
		}	
		
		// Debug
		printf("Server: Connection from %s port %s.\n",inet_ntoa(cli_addr.sin_addr), argv[1]);
			
		cout << "Creating Socket for Client..." << client_socket << endl;
			
		// Lock
		pthread_mutex_lock(&lock_state);
			
		// This send() function sends the 13 bytes of the string to the new server_socket
		send_message(client_socket, "Welcome! Currently, send me any command and I will respond.\n");
		
		// Unlock
		pthread_mutex_unlock(&lock_state);
			
		// Create Thread
		pthread_create(&client_thread, 0, respond, (void *)client_socket);
		pthread_detach(client_thread);
	}

	// Destory resources
    close(client_socket);
    close(server_socket);
	
    return 0; 
}