﻿// Skeleton written by Joe Zachary for CS 3500, September 2013
// Read the entire skeleton carefully and completely before you
// do anything else!

// Version 1.1 (9/22/13 11:45 a.m.)

// Change log:
//  (Version 1.1) Repaired mistake in GetTokens
//  (Version 1.1) Changed specification of second constructor to
//                clarify description of how validation works

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SpreadsheetUtilities
{
    /// <summary>
    /// Represents formulas written in standard infix notation using standard precedence
    /// rules.  The allowed symbols are non-negative numbers written using double-precision 
    /// floating-podouble syntax; variables that consist of a letter or underscore followed by 
    /// zero or more letters, underscores, or digits; parentheses; and the four operator 
    /// symbols +, -, *, and /.  
    /// 
    /// Spaces are significant only insofar that they delimit tokens.  For example, "xy" is
    /// a single variable, "x y" consists of two variables "x" and y; "x23" is a single variable; 
    /// and "x 23" consists of a variable "x" and a number "23".
    /// 
    /// Associated with every formula are two delegates:  a normalizer and a validator.  The
    /// normalizer is used to convert variables doubleo a canonical form, and the validator is used
    /// to add extra restrictions on the validity of a variable (beyond the standard requirement 
    /// that it consist of a letter or underscore followed by zero or more letters, underscores,
    /// or digits.)  Their use is described in detail in the constructor and method comments.
    /// </summary>
    public class Formula
    {
        // Variable Format Exception
        private readonly static string varFormatEx = "VariableFormatException, please check syntax for variable: ";
        private readonly static string syntaxEx = "SyntaxException, please check syntax for formula at location: ";
        private readonly static string gt0Ex = "ZeroLengthFormulaException, please check Formula length is greater than 0!";
        private readonly static string nullEx = "NullPointerException, please check that formula provided is not null!";


        // String Tokens
        private IEnumerable<string> tokens;
        private string formulaToEval;

        // Normalizer/Validator
        private Func<string, string> normalize;
        private Func<string, bool> validator;

        // Storage for variables/errors
        private HashSet<string> varVal;
        private string errorMsg;

        // Storage for finalRes, reset to null at beginning of method
        private object finalRes;

        /// <summary>
        /// Creates a Formula from a string that consists of an infix expression written as
        /// described in the class comment.  If the expression is syntactically invalid,
        /// throws a FormulaFormatException with an explanatory Message.
        /// 
        /// The associated normalizer is the identity function, and the associated validator
        /// maps every string to true.  
        /// </summary>
        public Formula(String formula) : this(formula, s => s, s => true) {}

        /// <summary>
        /// Creates a Formula from a string that consists of an infix expression written as
        /// described in the class comment.  If the expression is syntactically incorrect,
        /// throws a FormulaFormatException with an explanatory Message.
        /// 
        /// The associated normalizer and validator are the second and third parameters,
        /// respectively.  
        /// 
        /// If the formula contains a variable v such that normalize(v) is not a legal variable, 
        /// throws a FormulaFormatException with an explanatory message. 
        /// 
        /// If the formula contains a variable v such that isValid(normalize(v)) is false,
        /// throws a FormulaFormatException with an explanatory message.
        /// 
        /// Suppose that N is a method that converts all the letters in a string to upper case, and
        /// that V is a method that returns true only if a string consists of one letter followed
        /// by one digit.  Then:
        /// 
        /// new Formula("x2+y3", N, V) should succeed
        /// new Formula("x+y3", N, V) should throw an exception, since V(N("x")) is false
        /// new Formula("2x+y3", N, V) should throw an exception, since "2x+y3" is syntactically incorrect.
        /// </summary>
        public Formula(String formula, Func<string, string> normalize, Func<string, bool> isValid)
        {

            // Save formula etc.
            this.formulaToEval = formula;
            this.normalize = normalize;
            this.validator = isValid;

            // Initial checks
            if (Object.ReferenceEquals(this.formulaToEval,null))
            {
                this.errorMsg = nullEx;
                throw new FormulaFormatException(this.errorMsg);
            }

            // Length check
            if (this.formulaToEval.Length == 0)
            {
                this.errorMsg = gt0Ex;
                throw new FormulaFormatException(this.errorMsg);
            }

            // Initialization of variable storage/error msg
            varVal = new HashSet<string>();
            errorMsg = null;

            // Obtain tokens
            this.tokens = new List<string>();
            this.tokens = GetTokens(this.formulaToEval);

            // Validate tokens
            bool validTokens = areValidTokens(this.tokens, this.normalize, this.validator);

            // Throw error if needed
            if (validTokens) { }
            else
            {
                throw new FormulaFormatException(this.errorMsg);
            }
        }

        /// <summary>
        /// Evaluates this Formula, using the lookup delegate to determine the values of
        /// variables.  When a variable symbol v needs to be determined, it should be looked up
        /// via lookup(normalize(v)). (Here, normalize is the normalizer that was passed to 
        /// the constructor.)
        /// 
        /// For example, if L("x") is 2, L("X") is 4, and N is a method that converts all the letters 
        /// in a string to upper case:
        /// 
        /// new Formula("x+7", N, s => true).Evaluate(L) is 11
        /// new Formula("x+7").Evaluate(L) is 9
        /// 
        /// Given a variable symbol as its parameter, lookup returns the variable's value 
        /// (if it has one) or throws an ArgumentException (otherwise).
        /// 
        /// If no undefined variables or divisions by zero are encountered when evaluating 
        /// this Formula, the value is returned.  Otherwise, a FormulaError is returned.  
        /// The Reason property of the FormulaError should have a meaningful explanation.
        ///
        /// This method should never throw an exception.
        /// </summary>
        public object Evaluate(Func<string, double> lookup)
        {
            // Initialize final result
            this.finalRes = null;

            // Parsing and Evaluation Algorithm
            this.ps1Algo(this.tokens, lookup);

            // Return final result;
            return this.finalRes;
        }

        private object ps1Algo(IEnumerable<string> substrings, Func<string, double> variableEvaluator)
        {
            // Temp storage for arithmetic operations
            double temp1;
            double temp2;
            double finalTempVal;
            string tempOpr;
            double substrCount;

            // Initialize data structures, populate values
            Stack<double> valueStack = new Stack<double>();
            Stack<string> paramStack = new Stack<string>();

            // Get substrings length
            substrCount = substrings.Count<string>();
            double count = 0;

            foreach (string s in substrings)
            {
                count++;

                // Step 1 in Algorithm - double
                if (Regex.IsMatch(s, "(^\\d+\\.\\d*)|(^\\d*\\.\\d+)|(^\\d+)"))
                {
                    if (paramStack.Count == 0)
                    {
                        valueStack.Push(double.Parse(s));
                    }
                    else
                    {
                        if (paramStack.Peek().Equals("*") || paramStack.Peek().Equals("/"))
                        {
                            if (valueStack.Count == 0)
                            {
                                finalRes = new FormulaError("No values to operate on!");
                                return finalRes;
                            }
                            else
                            {
                                temp1 = valueStack.Pop();
                                temp2 = double.Parse(s);
                                tempOpr = paramStack.Pop();
                                if (tempOpr.Equals("*"))
                                {
                                    finalTempVal = temp1 * temp2;
                                }
                                else
                                {
                                    if (temp2 == 0)
                                    {
                                        finalRes = new FormulaError("Division by zero!");
                                        return finalRes;
                                    }
                                    else
                                    {
                                        finalTempVal = temp1 / temp2;
                                    }
                                }
                                valueStack.Push(finalTempVal);
                            }
                        }
                        else
                        {
                            valueStack.Push(double.Parse(s));
                        }
                    }

                }

                // Step 2 in PS1 Algo - Variable
                else if (Regex.IsMatch(s, "(^[a-zA-Z_]+[1-9][0-9]*$)"))
                {
                    try
                    {
                        temp2 = variableEvaluator(s);

                        if (Double.IsNaN(temp2))
                        {
                            this.finalRes = new FormulaError("Undefined Variable");
                            return finalRes;
                        }
                    }
                    catch (ArgumentException arg)
                    {
                        this.finalRes = new FormulaError("Undefined Variable");
                        return finalRes;
                    }

                    if (paramStack.Count == 0)
                    {
                        valueStack.Push(temp2);
                    }
                    else
                    {
                        if (paramStack.Peek().Equals("*") || paramStack.Peek().Equals("/"))
                        {
                            if (valueStack.Count == 0)
                            {
                                throw new System.DivideByZeroException();
                            }
                            else
                            {
                                temp1 = valueStack.Pop();
                                tempOpr = paramStack.Pop();
                                if (tempOpr.Equals("*"))
                                {
                                    finalTempVal = temp1 * temp2;
                                }
                                else if (temp2 == 0)
                                {
                                    finalRes = new FormulaError("Division by zero");
                                    return finalRes;
                                }
                                else
                                {
                                    finalTempVal = temp1 / temp2;
                                }
                                valueStack.Push(finalTempVal);
                            }
                        }
                        else
                        {
                            valueStack.Push(temp2);
                        }
                    }
                }

                // Step 3 in PS1 Algo - + or -
                else if (Regex.IsMatch(s, "(-)|(\\+)"))
                {
                    if (paramStack.Count == 0)
                    {
                        paramStack.Push(s);
                    }
                    else
                    {
                        if (paramStack.Peek().Equals("+") || paramStack.Peek().Equals("-"))
                        {
                            if (valueStack.Count < 2)
                            {
                                throw new System.DivideByZeroException();
                            }
                            else
                            {
                                temp2 = valueStack.Pop();
                                temp1 = valueStack.Pop();
                                tempOpr = paramStack.Pop();
                                if (tempOpr.Equals("+"))
                                {
                                    finalTempVal = temp1 + temp2;
                                }
                                else
                                {
                                    finalTempVal = temp1 - temp2;
                                }
                                valueStack.Push(finalTempVal);
                            }
                        }
                        paramStack.Push(s);
                    }
                }

                // Step 4 in PS1 Algo - * or /
                else if (Regex.IsMatch(s, "(/)|(\\*)"))
                {
                    paramStack.Push(s);
                }

                // Step 5 in PS1 Algo - (
                else if (Regex.IsMatch(s, "(\\()"))
                {
                    paramStack.Push(s);
                }

                // Step 6 in PS1 Algo - )
                else if (Regex.IsMatch(s, "(\\))"))
                {
                    if (paramStack.Count == 0)
                    {
                        throw new ArgumentException();
                    }
                    else
                    {
                        if (paramStack.Peek().Equals("+") || paramStack.Peek().Equals("-"))
                        {
                            if (valueStack.Count < 2)
                            {
                                throw new System.DivideByZeroException();
                            }
                            else
                            {
                                temp2 = valueStack.Pop();
                                temp1 = valueStack.Pop();
                                tempOpr = paramStack.Pop();
                                if (tempOpr.Equals("+"))
                                {
                                    finalTempVal = temp1 + temp2;
                                }
                                else
                                {
                                    finalTempVal = temp1 - temp2;
                                }
                                valueStack.Push(finalTempVal);

                                if (paramStack.Count > 0 && paramStack.Peek().Equals("("))
                                {
                                    paramStack.Pop();
                                }

                                if (paramStack.Count == 0) { }
                                else
                                {
                                    if (paramStack.Peek().Equals("*") || paramStack.Peek().Equals("/"))
                                    {
                                        if (valueStack.Count < 2)
                                        {
                                            throw new System.DivideByZeroException();
                                        }
                                        else
                                        {
                                            temp2 = valueStack.Pop();
                                            temp1 = valueStack.Pop();
                                            tempOpr = paramStack.Pop();
                                            if (tempOpr.Equals("*"))
                                            {
                                                finalTempVal = temp1 * temp2;
                                            }
                                            else if (temp2 == 0)
                                            {
                                                finalRes = new FormulaError("Division by zero!");
                                                return finalRes;
                                            }
                                            else
                                            {
                                                finalTempVal = temp1 / temp2;
                                            }
                                            valueStack.Push(finalTempVal);
                                        }
                                    }
                                }
                            }
                        }
                        else if (paramStack.Peek().Equals("("))
                        {
                            paramStack.Pop();
                        }
                    }
                }

                // Final Step 1 in PS1 Algo - if oprStack is empty
                if (count == substrCount && paramStack.Count == 0 && valueStack.Count == 1)
                {
                    this.finalRes = valueStack.Pop();
                    return this.finalRes;
                }
                // Final Step 2 in PS1 Algo - if oprStack is not empty
                else if (count == substrCount && paramStack.Count == 1 && valueStack.Count == 2)
                {
                    temp2 = valueStack.Pop();
                    temp1 = valueStack.Pop();
                    tempOpr = paramStack.Pop();

                    if (tempOpr.Equals("*"))
                    {
                        this.finalRes = temp1 * temp2;
                    }
                    else if (tempOpr.Equals("-"))
                    {
                        this.finalRes = temp1 - temp2;
                    }
                    if (tempOpr.Equals("+"))
                    {
                        this.finalRes = temp1 + temp2;
                    }
                    else if (tempOpr.Equals("/"))
                    {
                        this.finalRes = temp1 / temp2;
                    }
                }
            }

            return this.finalRes;
        }

        /// <summary>
        /// Enumerates the normalized versions of all of the variables that occur in this 
        /// formula.  No normalization may appear more than once in the enumeration, even 
        /// if it appears more than once in this Formula.
        /// 
        /// For example, if N is a method that converts all the letters in a string to upper case:
        /// 
        /// new Formula("x+y*z", N, s => true).GetVariables() should enumerate "X", "Y", and "Z"
        /// new Formula("x+X*z", N, s => true).GetVariables() should enumerate "X" and "Z".
        /// new Formula("x+X*z").GetVariables() should enumerate "x", "X", and "z".
        /// </summary>
        public IEnumerable<String> GetVariables()
        {
            if (varVal.Count > 0)
            {
                foreach (string s in this.varVal)
                {
                    yield return s;
                }
            }

        }

        /// <summary>
        /// Returns a string containing no spaces which, if passed to the Formula
        /// constructor, will produce a Formula f such that this.Equals(f).  All of the
        /// variables in the string should be normalized.
        /// 
        /// For example, if N is a method that converts all the letters in a string to upper case:
        /// 
        /// new Formula("x + y", N, s => true).ToString() should return "X+Y"
        /// new Formula("x + Y").ToString() should return "x+Y"
        /// </summary>
        public override string ToString()
        {
            string result = null;
            foreach (string s in this.tokens)
            {
                result += s;
            }
            return result;
        }

        /// <summary>
        /// If obj is null or obj is not a Formula, returns false.  Otherwise, reports
        /// whether or not this Formula and obj are equal.
        /// 
        /// Two Formulae are considered equal if they consist of the same tokens in the
        /// same order.  To determine token equality, all tokens are compared as strings 
        /// except for numeric tokens, which are compared as doubles, and variable tokens,
        /// whose normalized forms are compared as strings.
        /// 
        /// For example, if N is a method that converts all the letters in a string to upper case:
        ///  
        /// new Formula("x1+y2", N, s => true).Equals(new Formula("X1  +  Y2")) is true
        /// new Formula("x1+y2").Equals(new Formula("X1+Y2")) is false
        /// new Formula("x1+y2").Equals(new Formula("y2+x1")) is false
        /// new Formula("2.0 + x7").Equals(new Formula("2.000 + x7")) is true
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!Object.ReferenceEquals(obj, null))
            {
                if (Object.ReferenceEquals(obj.GetType(), this.GetType()))
                {
                    // Object is a Formula type
                    Formula comparator = (Formula)obj;

                    // Comparison
                    bool retVal = true;

                    // First step
                    if (this.tokens.Count<string>().Equals(comparator.tokens.Count<string>()))
                    {
                        // Second step
                        for (int i = 0; i < this.tokens.Count<string>(); i++)
                        {
                            // Try to parse the data  
                            double compVal;
                            if (double.TryParse(this.tokens.ElementAt<string>(i), out compVal))
                            {
                                double secondCompVal;
                                if (double.TryParse(comparator.tokens.ElementAt<string>(i), out secondCompVal))
                                {
                                    retVal = compVal.Equals(secondCompVal);
                                }
                            }
                            else
                            {
                                retVal = this.tokens.ElementAt<string>(i).Equals(comparator.tokens.ElementAt<string>(i));
                            }
                            if (retVal.Equals(false))
                            {
                                return retVal;
                            }
                        }
                        return retVal;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        /// <summary>
        /// Reports whether f1 == f2, using the notion of equality from the Equals method.
        /// Note that if both f1 and f2 are null, this method should return true.  If one is
        /// null and one is not, this method should return false.
        /// </summary>
        public static bool operator ==(Formula f1, Formula f2)
        {
            if (Object.ReferenceEquals(f1, null) && Object.ReferenceEquals(f2, null))
            {
                return true;
            }
            if (Object.ReferenceEquals(f1, null) && !Object.ReferenceEquals(f2, null))
            {
                return false;
            }
            if (!Object.ReferenceEquals(f1, null) && Object.ReferenceEquals(f2, null))
            {
                return false;
            }
            else
            {
                return f1.Equals(f2);
            }
        }

        /// <summary>
        /// Reports whether f1 != f2, using the notion of equality from the Equals method.
        /// Note that if both f1 and f2 are null, this method should return false.  If one is
        /// null and one is not, this method should return true.
        /// </summary>
        public static bool operator !=(Formula f1, Formula f2)
        {
            if (Object.ReferenceEquals(f1, null) && Object.ReferenceEquals(f2, null))
            {
                return false;
            }
            if (Object.ReferenceEquals(f1, null) && !Object.ReferenceEquals(f2, null))
            {
                return true;
            }
            if (!Object.ReferenceEquals(f1, null) && Object.ReferenceEquals(f2, null))
            {
                return true;
            }
            else
            {
                return !f1.Equals(f2);
            }
        }

        /// <summary>
        /// Returns a hash code for this Formula.  If f1.Equals(f2), then it must be the
        /// case that f1.GetHashCode() == f2.GetHashCode().  Ideally, the probability that two 
        /// randomly-generated unequal Formulae have the same hash code should be extremely small. Calculated
        /// by examining the ASCII bytes used to initiate each formula.
        /// </summary>
        public override int GetHashCode()
        {
            int hashCode = 0;
            if (!Object.ReferenceEquals(this, null))
            {
                byte[] asciiBytes = Encoding.ASCII.GetBytes(this.formulaToEval.Replace(" ", ""));
                foreach (byte b in asciiBytes)
                {
                    hashCode = hashCode + b;
                }
                return hashCode;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Given an expression, enumerates the tokens that compose it.  Tokens are left paren;
        /// right paren; one of the four operator symbols; a string consisting of a letter or underscore
        /// followed by zero or more letters, digits, or underscores; a double literal; and anything that doesn't
        /// match one of those patterns.  There are no empty tokens, and no token contains white space.
        /// </summary>
        private static IEnumerable<string> GetTokens(String formula)
        {
            // Patterns for individual tokens
            String lpPattern = @"\(";
            String rpPattern = @"\)";
            String opPattern = @"[\+\-*/]";
            String varPattern = @"[a-zA-Z_](?: [a-zA-Z_]|\d)*";
            String doublePattern = @"(?: \d+\.\d* | \d*\.\d+ | \d+ ) (?: [eE][\+-]?\d+)?";
            String spacePattern = @"\s+";

            // Overall pattern
            String pattern = String.Format("({0}) | ({1}) | ({2}) | ({3}) | ({4}) | ({5})",
                                            lpPattern, rpPattern, opPattern, varPattern, doublePattern, spacePattern);

            // Enumerate matching tokens that don't consist solely of white space.
            foreach (String s in Regex.Split(formula, pattern, RegexOptions.IgnorePatternWhitespace))
            {
                if (!Regex.IsMatch(s, @"^\s*$", RegexOptions.Singleline))
                {
                    yield return s;
                }
            }

        }

        /// <summary>
        /// Validate input tokens
        /// </summary>
        /// <param name="substrings">Parsed input tokens</param>
        /// <param name="normalize">Normalizer delgate</param>
        /// <param name="isValid">Validation method from user</param>
        /// <returns>Whether tokens are valid, bool</returns>
        private bool areValidTokens(IEnumerable<string> substrings, Func<string, string> normalize, Func<string, bool> isValid)
        {
            // Code has been borrowed from private GetTokens method
            List<string> replaceValues = new List<string>();

            // Patterns for individual tokens
            String lpPattern = @"\(";
            String rpPattern = @"\)";
            String opPattern = "^[\\+\\-*/]";
            String varPattern = "(^[a-zA-Z]+[1-9][0-9]*$)";
            String doublePattern = "(^\\d+\\.\\d*)|(^\\d*\\.\\d+)|(^\\d+)";

            // Create regex to handle all cases of valid input
            Regex lp = new Regex(lpPattern);
            Regex rp = new Regex(rpPattern);
            Regex op = new Regex(opPattern);
            Regex var = new Regex(varPattern);
            Regex doubl = new Regex(doublePattern);

            Match varMatch = null;

            // Init return value
            bool retVal = true;

            // Init var/opr/double count
            Dictionary<string, int> counters = new Dictionary<string, int>();
            string lastString = "";
            int count = 0;

            // Validate each token, save overall bool
            foreach (string s in substrings)
            {
                
                // Check first string
                if (count == 0)
                {
                    if (!(Regex.IsMatch(s, lpPattern) || Regex.IsMatch(s, varPattern) || Regex.IsMatch(s, doublePattern)))
                    {
                        this.errorMsg = syntaxEx + s;
                        throw new FormulaFormatException(this.errorMsg);
                    }
                }

                // Check for opening paren syntax
                if (lastString.Equals("("))
                {
                    if (!(Regex.IsMatch(s, lpPattern) || Regex.IsMatch(s, varPattern) || Regex.IsMatch(s, doublePattern)))
                    {
                        this.errorMsg = syntaxEx + s;
                        throw new FormulaFormatException(this.errorMsg);
                    }
                }

                // Check for closing paren syntax
                if (Regex.IsMatch(lastString, doublePattern) || Regex.IsMatch(lastString, varPattern) || Regex.IsMatch(lastString, rpPattern))
                {
                    if (!(Regex.IsMatch(s, rpPattern) || Regex.IsMatch(s, opPattern)))
                    {
                        this.errorMsg = syntaxEx + s;
                        throw new FormulaFormatException(this.errorMsg);
                    }
                }

                // Check for double operators
                if (Regex.IsMatch(lastString, opPattern) && Regex.IsMatch(s, opPattern))
                {
                        this.errorMsg = syntaxEx + s;
                        throw new FormulaFormatException(this.errorMsg);
                }

                // Watching for variables
                varMatch = Regex.Match(s, varPattern);

                // Verifying that variables are valid based on Normalizer and Validator delegates
                if (varMatch.Success)
                {
                    string replace = normalize(s);
                    if (isValid(replace))
                    {
                        this.varVal.Add(replace);
                        replaceValues.Add(replace);
                        lastString = s;
                        if (counters.ContainsKey(s))
                        {
                            counters[s] = counters[s] + 1;
                        }
                        else
                        {
                            counters.Add(s, 1);
                        }
                        count++;
                        continue;
                    }
                    else
                    {
                        retVal = false;
                        this.errorMsg = varFormatEx + s;
                        break;
                    }
                }

                // Check regular tokens
                retVal = lp.IsMatch(s);
                if (retVal.Equals(false))
                {
                    // Check regular tokens
                    retVal = rp.IsMatch(s);
                    if (retVal.Equals(false))
                    {
                        // Check regular tokens
                        retVal = op.IsMatch(s);
                        if (retVal.Equals(false))
                        {
                            // Check regular tokens
                            retVal = doubl.IsMatch(s);
                            if (retVal.Equals(false))
                            {
                                this.errorMsg = syntaxEx + s;
                                break;
                            }
                            else if (counters.ContainsKey(s))
                            {
                                counters[s] = counters[s] + 1;
                            }
                            else
                            {
                                counters.Add(s, 1);
                            }
                        }
                        else if (counters.ContainsKey(s))
                        {
                            counters[s] = counters[s] + 1;
                        }
                        else
                        {
                            counters.Add(s, 1);
                        }
                    }
                    else if (counters.ContainsKey(s))
                    {
                        counters[s] = counters[s] + 1;
                    }
                    else
                    {
                        counters.Add(s, 1);
                    }
                }
                else if (retVal.Equals(true))
                {
                    if (counters.ContainsKey(s))
                    {
                        counters[s] = counters[s] + 1;
                    }
                    else
                    {
                        counters.Add(s, 1);
                    }
                }
                
                if (counters.Count > 0 && counters.ContainsKey("(") && s.Equals(")"))
                {
                    if (counters[")"] > counters["("] )
                    {
                        this.errorMsg = syntaxEx + s;
                        throw new FormulaFormatException(this.errorMsg);
                    }
                }
                if (counters.Count > 0 && !counters.ContainsKey("(") && s.Equals(")"))
                {
                        this.errorMsg = syntaxEx + s;
                        throw new FormulaFormatException(this.errorMsg);
                }
                lastString = s;
                count++;
                replaceValues.Add(s);
            }

            // Error handling
            if (!(Regex.IsMatch(lastString, rpPattern) || Regex.IsMatch(lastString, varPattern) || Regex.IsMatch(lastString, doublePattern)))
            {
                this.errorMsg = syntaxEx + lastString;
                throw new FormulaFormatException(this.errorMsg);
            }
            if (counters.Count > 0 && counters.ContainsKey(")") && counters.ContainsKey("("))
            {
                if (counters[")"] != counters["("])
                {
                    this.errorMsg = syntaxEx + "() count off";
                    throw new FormulaFormatException(this.errorMsg);
                }
            }
            this.tokens = replaceValues;
            return retVal;
        }
    }

    /// <summary>
    /// Used to report syntactic errors in the argument to the Formula constructor.
    /// </summary>
    public class FormulaFormatException : Exception
    {
        /// <summary>
        /// Constructs a FormulaFormatException containing the explanatory message.
        /// </summary>
        public FormulaFormatException(String message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Used as a possible return value of the Formula.Evaluate method.
    /// </summary>
    public struct FormulaError
    {
        /// <summary>
        /// Constructs a FormulaError containing the explanatory reason.
        /// </summary>
        /// <param name="reason"></param>
        public FormulaError(String reason)
            : this()
        {
            Reason = reason;
        }

        /// <summary>
        ///  The reason why this FormulaError was created.
        /// </summary>
        public string Reason { get; private set; }
    }
}

