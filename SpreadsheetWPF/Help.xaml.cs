﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
using System.IO;

namespace SpreadsheetWPF
{
    /// <summary>
    /// Interaction logic for Help.xaml
    /// </summary>
    public partial class Help : Window
    {
        public Help()
        {
            InitializeComponent();

            // Readme document
            String basepath = Directory.GetCurrentDirectory() + @"\README.xps";
            XpsDocument document1 = new XpsDocument(@""+basepath, System.IO.FileAccess.Read);
            this.readme.Document = document1.GetFixedDocumentSequence();
        }
    }
}
