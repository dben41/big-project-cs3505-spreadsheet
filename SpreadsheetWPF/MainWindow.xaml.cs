﻿using System;
using SS;
using SpreadsheetUtilities;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SpreadsheetWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Storage for the SpreadsheetPanel
        private AbstractSpreadsheet spreadsheetData = new Spreadsheet(s => true, s => s.ToUpper(), "PS6");

        // Previous col and row for formulaChecker
        private int prevCol = 0, prevRow = 0;

        // Filename abstraction
        private string spreadsheetFilename = "";

        public MainWindow()
        {
            // General
            InitializeComponent();

            // Add event listeners to WFH Child (SpreadsheetPanel)
            (wfhSpreadsheet.Child as SpreadsheetPanel).SelectionChanged += displaySelection;

            // Arrow key monitors
            this.KeyDown += new System.Windows.Input.KeyEventHandler(OnWindowButtonKey);

        }

        /// <summary>
        /// Listens for arrow keys.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowButtonKey(object sender, System.Windows.Input.KeyEventArgs e)
        {
            // Selection values
            int row, col;

            // Preprocessing
            (wfhSpreadsheet.Child as SpreadsheetPanel).GetSelection(out col, out row);  

            // Key Functionality
            switch (e.Key)
            {
                case Key.Down:
                    if (row != 98)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetSelection(col, row + 1);
                        displaySelection((wfhSpreadsheet.Child as SpreadsheetPanel));
                    }
                    break;

                case Key.Up:
                    if (row != 0)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetSelection(col, row - 1);
                        displaySelection((wfhSpreadsheet.Child as SpreadsheetPanel));
                    }
                    break;

                case Key.Right:
                    if (col != 25)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetSelection(col + 1, row);
                        displaySelection((wfhSpreadsheet.Child as SpreadsheetPanel));
                    }
                    break;

                case Key.Left:
                    if (col != 0)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetSelection(col - 1, row);
                        displaySelection((wfhSpreadsheet.Child as SpreadsheetPanel));
                    }
                    break;

                case Key.Tab:
                    if (col != 25)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetSelection(col + 1, row);
                        displaySelection((wfhSpreadsheet.Child as SpreadsheetPanel));
                    }
                    e.Handled = true;
                    break;

                case Key.Enter:
                    if (row != 98)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetSelection(col, row + 1);
                        displaySelection((wfhSpreadsheet.Child as SpreadsheetPanel));
                    }
                    e.Handled = true;
                    break;

                default:
                    this.cellCntTextBox.Focus();
                    return;
            }

        }

        /// <summary>
        /// Opens a File Dialog window for opening a spreadsheet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_File_Dialog(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Spreadsheet"; // Default file name
            dlg.DefaultExt = ".ss"; // Default file extension
            dlg.Filter = "Spreadsheet Documents (.ss)|*.ss|All Files (*.*)|*"; // Filter files by extension 
            dlg.FilterIndex = 1;

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                this.spreadsheetData = new Spreadsheet(filename, s => true, s => s.ToUpper(), "PS6");

                // Clear way for new spreadsheet
                (wfhSpreadsheet.Child as SpreadsheetPanel).Clear();

                // Reload it
                this.Reload_SpreadsheetPanel();
                this.spreadsheetFilename = filename;
                displaySelection(wfhSpreadsheet.Child as SpreadsheetPanel);

            }
        }

        /// <summary>
        /// Saves the current spreadsheet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_File_Dialog(object sender, RoutedEventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Spreadsheet"; // Default file name
            dlg.DefaultExt = ".ss"; // Default file extension
            dlg.Filter = "Spreadsheet Documents (.ss)|*.ss|All Files (*.*)|*"; // Filter files by extension 
            dlg.FilterIndex = 1;

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                if (dlg.FilterIndex == 2)
                {
                    dlg.DefaultExt = "";
                }

                // Save document 
                string filename = dlg.FileName;
                this.spreadsheetData.Save(filename);

                // Was save successful
                String msg = "Save was successful!";
                System.Windows.MessageBox.Show(msg, "Save Successful", MessageBoxButton.OK, MessageBoxImage.Information);
                this.spreadsheetFilename = filename;
            }

        }


        /// <summary>
        /// Saves the current spreadsheet judging what type of save to perform.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_File(object sender, RoutedEventArgs e)
        {
            if (this.spreadsheetFilename.Equals(""))
            {
                Save_File_Dialog(sender, e);
            }
            else
            {
                this.spreadsheetData.Save(this.spreadsheetFilename);
            }
        }

        /// <summary>
        /// Verify that this close is legal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Window_Check(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // If data is dirty, notify user and ask for a response 
            if (this.spreadsheetData.Changed)
            {
                string msg = "Data has not been saved. Do you wish to close without saving?";
                MessageBoxResult result = System.Windows.MessageBox.Show(msg, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
            else
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// Closes the current window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Window(object sender, RoutedEventArgs e)
        {

            // If data is dirty, notify user and ask for a response 
            if (this.spreadsheetData.Changed)
            {
                string msg = "Data has not been saved. Do you wish to close without saving?";
                MessageBoxResult result = System.Windows.MessageBox.Show(msg, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    // If user doesn't want to close, cancel closure
                }
                else
                {
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }

        }

        /// <summary>
        /// Creates new spreadsheet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void New_Spreadsheet(object sender, RoutedEventArgs e)
        {
            // Create a new Main Window
            MainWindow mw = new MainWindow();

            // Clear data
            (mw.wfhSpreadsheet.Child as SpreadsheetPanel).Clear();

            // Show new window
            mw.Show();
        }

        /// <summary>
        /// Shows About Page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Show_About(object sender, RoutedEventArgs e)
        {
            // Create a new Main Window
            About about = new About();

            // Show new window
            about.Show();
        }

        /// <summary>
        /// Shows Help Page with ReadMe file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Show_Help(object sender, RoutedEventArgs e)
        {
            // Create a new Main Window
            Help help = new Help();

            // Show new window
            help.Show();
        }

        /// <summary>
        /// Displays selection and updates values etc.
        /// </summary>
        /// <param name="ss"></param>
        private void displaySelection(SpreadsheetPanel ss)
        {
            // Selection values
            int row, col;
            String value;
            ss.GetSelection(out col, out row);
            ss.GetValue(col, row, out value);

            // Preprocessing
            // Check if previous cell was formula
            this.Formula_Checker(prevCol, prevRow);
            this.prevCol = col;
            this.prevRow = row;

            // Change Cell Name Label
            cellNmLbl.Content = "" + ColumnNumberToLetter(col) + (row + 1);

            // Change Cell Value Label
            cellValLbl.Content = value;

            // Populate Cell Contents
            Object contents = this.spreadsheetData.GetCellContents("" + ColumnNumberToLetter(col) + (row + 1));
            if (((System.Type)contents.GetType()) == typeof(Formula))
            {
                cellCntTextBox.Text = "=" + contents.ToString();
            }
            else
            {
                cellCntTextBox.Text = contents.ToString();
            }
        }

        /// <summary>
        ///  Return the letter for this column number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private char ColumnNumberToLetter(int number)
        {
            // See if it's out of bounds.
            if (number < 0) return 'A';
            if (number > 25) return 'Z';

            // Calculate the letter.
            return (char)(number + (int)'A');
        }

        /// <summary>
        /// Column number conversion
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private int GetColumnNumber(string name)
        {
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }

            return number - 1;
        }

        /// <summary>
        /// Updates Cell contents.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Update_Cell_Contents(object sender, TextChangedEventArgs e)
        {
            // Get cell name
            String cellName = (String)this.cellNmLbl.Content;

            if (!this.cellCntTextBox.Text.Replace(" ", "").StartsWith("=") && !this.spreadsheetData.GetCellContents(cellName).ToString().StartsWith("="))
            {
                if (this.spreadsheetData.GetCellContents(cellName).ToString().Equals(this.cellCntTextBox.Text)) { }
                else
                {
                    // Update database
                    HashSet<String> recalculate = new HashSet<string>(this.spreadsheetData.SetContentsOfCell(cellName, this.cellCntTextBox.Text));

                    // Populate Model
                    this.cellValLbl.Content = this.spreadsheetData.GetCellValue(cellName).ToString();

                    // Final Updates
                    foreach (string recalculatedCell in recalculate)
                    {
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetValue(
                            GetColumnNumber("" + recalculatedCell.ElementAt(0)),
                            int.Parse(recalculatedCell.Substring(1, recalculatedCell.Length - 1)) - 1,
                            "" + this.spreadsheetData.GetCellValue(recalculatedCell));
                    }
                }
            }
        }

        /// <summary>
        /// Helper method for reloading spreadsheet.
        /// </summary>
        private void Reload_SpreadsheetPanel()
        {
            foreach (string key in (this.spreadsheetData.GetNamesOfAllNonemptyCells()))
            {

                // Final Updates
                (wfhSpreadsheet.Child as SpreadsheetPanel).SetValue(
                    GetColumnNumber("" + key.ElementAt(0)),
                    int.Parse(key.Substring(1, key.Length - 1)) - 1,
                    "" + this.spreadsheetData.GetCellValue(key));
            }
        }

        /// <summary>
        /// Checks if Formula Exists when another cell is clicked
        /// </summary>
        private void Formula_Checker(int prevCol, int prevRow)
        {
            if (this.cellCntTextBox.Text.Replace(" ", "").StartsWith("="))
            {
                // Get cell name
                String cellName = "" + ColumnNumberToLetter(prevCol) + (prevRow + 1);

                // Check for redundancy 
                if (this.spreadsheetData.GetCellContents(cellName).ToString().Equals(this.cellCntTextBox.Text.Replace(" ", "").Substring(1, this.cellCntTextBox.Text.Replace(" ", "").Length - 1))) { }
                else
                {
                    try
                    {
                        // Update database
                        this.spreadsheetData.SetContentsOfCell(cellName, this.cellCntTextBox.Text);
                    }
                    catch (FormulaFormatException e)
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show(e.Message + " (CELL:" + cellName + ")", "Formula Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    finally
                    {
                        // Populate Model
                        this.cellValLbl.Content = this.spreadsheetData.GetCellValue(cellName).ToString();

                        // Final Updates
                        (wfhSpreadsheet.Child as SpreadsheetPanel).SetValue(
                            prevCol,
                            prevRow,
                            "" + this.spreadsheetData.GetCellValue(cellName).ToString());
                    }
                }
            }
        }
    }
}
