﻿// Skeleton implementation written by Joe Zachary for CS 3500, September 2013.
// Version 1.1 (Fixed error in comment for RemoveDependency.)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace SpreadsheetUtilities
{

    /// <summary>
    /// A DependencyGraph can be modeled as a set of ordered pairs of strings.  Two ordered pairs
    /// (s1,t1) and (s2,t2) are considered equal if and only if s1 equals s2 and t1 equals t2.
    /// (Recall that sets never contain duplicates.  If an attempt is made to add an element to a 
    /// set, and the element is already in the set, the set remains unchanged.)
    /// 
    /// Given a DependencyGraph DG:
    /// 
    ///    (1) If s is a string, the set of all strings t such that (s,t) is in DG is called dependents(s).
    ///        
    ///    (2) If s is a string, the set of all strings t such that (t,s) is in DG is called dependees(s).
    ///
    /// For example, suppose DG = {("a", "b"), ("a", "c"), ("b", "d"), ("d", "d")}
    ///     dependents("a") = {"b", "c"}
    ///     dependents("b") = {"d"}
    ///     dependents("c") = {}
    ///     dependents("d") = {"d"}
    ///     dependees("a") = {}
    ///     dependees("b") = {"a"}
    ///     dependees("c") = {"a"}
    ///     dependees("d") = {"b", "d"}
    /// </summary>
    public class DependencyGraph
    {

        // Private representation of a Dependency Graph
        private Dictionary<string, HashSet<string>> depGraph;

        /// <summary>
        /// Creates an empty DependencyGraph.
        /// </summary>
        public DependencyGraph()
        {
            this.depGraph = new Dictionary<string, HashSet<string>>();
        }

        /// <summary>
        /// The number of ordered pairs in the DependencyGraph.
        /// </summary>
        public int Size
        {
            get
            {
                // Get property
                if (this.depGraph.Count == 0)
                {
                    return 0;
                }
                else
                {
                    // Count the size of each entries HashSet
                    int count = 0;
                    foreach (string temp in this.depGraph.Keys)
                    {
                        count += this.depGraph[temp].Count;
                    }
                    return count;
                }
            }
        }


        /// <summary>
        /// The size of dependees(s).
        /// This property is an example of an indexer.  If dg is a DependencyGraph, you would
        /// invoke it like this:
        /// dg["a"]
        /// It should return the size of dependees("a")
        /// </summary>
        public int this[string s]
        {
            get
            {
                // Count number of dependees in DG
                int count = 0;
                foreach (string temp in this.depGraph.Keys)
                {
                    if (this.depGraph[temp].Contains(s))
                    {
                        count++;
                    }
                }
                return count;
            }
        }


        /// <summary>
        /// Reports whether dependents(s) is non-empty.
        /// </summary>
        public bool HasDependents(string s)
        {
            if (this.depGraph.Count > 0)
            {
                // Validate existence and depGraphs count in unison
                if (this.depGraph.ContainsKey(s) == true && this.depGraph[s].Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Reports whether dependees(s) is non-empty.
        /// </summary>
        public bool HasDependees(string s)
        {
            // Is graph empty
            if (this.depGraph.Count > 0)
            {
                // Does graph contain s
                foreach (string temp in this.depGraph.Keys)
                {
                    if (depGraph[temp].Contains(s))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Enumerates dependents(s).
        /// </summary>
        public IEnumerable<string> GetDependents(string s)
        {
            // Does depGraph contain the key
            if (this.depGraph.ContainsKey(s))
            {
                // Yield dependents
                foreach (String temp in this.depGraph[s])
                {
                    yield return temp;
                }
            }
        }

        /// <summary>
        /// Enumerates dependees(s).
        /// </summary>
        public IEnumerable<string> GetDependees(string s)
        {
            foreach (string temp in this.depGraph.Keys)
            {
                // Validate dependee-dependent relationship
                if (depGraph[temp].Contains(s))
                {
                    yield return temp;
                }
            }
        }


        /// <summary>
        /// Adds the ordered pair (s,t), if it doesn't exist
        /// </summary>
        /// <param name="s"></param>
        /// <param name="t"></param>
        public void AddDependency(string s, string t)
        {
            // Contains == true
            if (this.depGraph.ContainsKey(s))
            {
                this.depGraph[s].Add(t);
            }
            else
            {
                // Need new HashSet
                this.depGraph[s] = new HashSet<string>();
                this.depGraph[s].Add(t);
            }
        }


        /// <summary>
        /// Removes the ordered pair (s,t), if it exists
        /// </summary>
        /// <param name="s"></param>
        /// <param name="t"></param>
        public void RemoveDependency(string s, string t)
        {
            // Contains == true
            if (this.depGraph.ContainsKey(s))
            {
                this.depGraph[s].Remove(t);
            }
        }


        /// <summary>
        /// Removes all existing ordered pairs of the form (s,r).  Then, for each
        /// t in newDependents, adds the ordered pair (s,t).
        /// </summary>
        public void ReplaceDependents(string s, IEnumerable<string> newDependents)
        {
            // Contains == true
            if (this.depGraph.ContainsKey(s))
            {
                // Replace pointer in heap to new object
                this.depGraph[s] = new HashSet<string>();
                foreach (string insert in newDependents)
                {
                    this.depGraph[s].Add(insert);
                }
            }
        }


        /// <summary>
        /// Removes all existing ordered pairs of the form (r,s).  Then, for each 
        /// t in newDependees, adds the ordered pair (t,s).
        /// </summary>
        public void ReplaceDependees(string s, IEnumerable<string> newDependees)
        {
            // Check for s in every HashSet
            foreach (HashSet<string> delete in this.depGraph.Values)
            {
                // Delete s if exists
                if (delete.Contains(s))
                {
                    delete.Remove(s);
                }
            }

            // New relationships
            foreach (string replace in newDependees)
            {
                // Add new key with s
                if (this.depGraph.ContainsKey(replace))
                {
                    this.depGraph[replace].Add(s);
                }
                else
                {
                    this.depGraph[replace] = new HashSet<string>();
                    this.depGraph[replace].Add(s);
                }
            }
        }
    }
}


