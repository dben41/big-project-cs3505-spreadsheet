﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SS
{

    /// <summary>
    /// Cell Class
    /// Appropriately represents a cell in a spreadsheet object. Cells are represented by a name, content and value.
    /// Names are appropriately defined with the following definition taken from the Spreadsheet Class.
    /// 
    /// A string is a valid cell name if and only if:
    ///   (1) its first character is an underscore or a letter
    ///   (2) its remaining characters (if any) are underscores and/or letters and/or digits
    /// Note that this is the same as the definition of valid variable from the PS3 Formula class.
    ///
    /// For example, "x", "_", "x2", "y_15", and "___" are all valid cell  names, but
    /// "25", "2x", and "&" are not.  Cell names are case sensitive, so "x" and "X" are
    /// different cell names.
    /// The value of a cell can be (1) a string, (2) a double, or (3) a FormulaError.  
    /// (By analogy, the value of an Excel cell is what is displayed in that cell's position
    /// in the grid.)
    /// If a cell's contents is a string, its value is that string.
    /// If a cell's contents is a double, its value is that double.
    /// If a cell's contents is a Formula, its value is either a double or a FormulaError,
    /// as reported by the Evaluate method of the Formula class.  The value of a Formula,
    /// of course, can depend on the values of variables.  The value of a variable is the
    /// value of the spreadsheet cell it names (if that cell's value is a double) or
    /// is undefined (otherwise).
    /// </summary>
    public class Cell
    {

        /// <summary>
        /// Name of cell
        /// </summary>
        private String name;

        /// <summary>
        /// Name setter/getter
        /// </summary>
        public String Name { get { return name; } set { this.name = value; } }
        
        /// <summary>
        /// Contents of cell
        /// </summary>
        private Object contents;

        /// <summary>
        /// Contents setter/getter
        /// </summary>
        public Object Contents { get { return contents; } set { this.contents = value; } }
        
        /// <summary>
        /// Cell value
        /// </summary>
        private Object val;

        /// <summary>
        /// Value setter/getter
        /// </summary>
        public Object Val { get { return val; } set { this.val = value; } }

        /// <summary>
        /// Cell constructor
        /// </summary>
        /// <param name="name">Name of cell</param>
        /// <param name="contents">Cell contents</param>
        /// <param name="value">Cell value</param>
        public Cell(String name, Object contents, Object value)
        {
            this.name = name;
            this.contents = contents;
            this.val = value;
        }

        /// <summary>
        /// This method is to validate whether or not a cell is empty
        /// </summary>
        /// <returns>Whether or not the cell is empty</returns>
        public bool isEmpty()
        {
            // Get object type
            System.Type typ = this.contents.GetType();
            if (typeof(String) == typ)
            {
                // Check to see if string is empty
                String str = (string)this.contents;
                if (str.Length > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            // If not a string
            else
            {
                return false;
            }
        }
    }
}