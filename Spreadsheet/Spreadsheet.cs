﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpreadsheetUtilities;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;


namespace SS
{

    // PARAGRAPHS 2 and 3 modified for PS5.
    /// <summary>
    /// An AbstractSpreadsheet object represents the state of a simple spreadsheet.  A 
    /// spreadsheet consists of an infinite number of named cells.
    /// 
    /// A string is a cell name if and only if it consists of one or more letters,
    /// followed by one or more digits AND it satisfies the predicate IsValid.
    /// For example, "A15", "a15", "XY032", and "BC7" are cell names so long as they
    /// satisfy IsValid.  On the other hand, "Z", "X_", and "hello" are not cell names,
    /// regardless of IsValid.
    /// 
    /// Any valid incoming cell name, whether passed as a parameter or embedded in a formula,
    /// must be normalized with the Normalize method before it is used by or saved in 
    /// this spreadsheet.  For example, if Normalize is s => s.ToUpper(), then
    /// the Formula "x3+a5" should be converted to "X3+A5" before use.
    /// 
    /// A spreadsheet contains a cell corresponding to every possible cell name.  
    /// In addition to a name, each cell has a contents and a value.  The distinction is
    /// important.
    /// 
    /// The contents of a cell can be (1) a string, (2) a double, or (3) a Formula.  If the
    /// contents is an empty string, we say that the cell is empty.  (By analogy, the contents
    /// of a cell in Excel is what is displayed on the editing line when the cell is selected.)
    /// 
    /// In a new spreadsheet, the contents of every cell is the empty string.
    ///  
    /// The value of a cell can be (1) a string, (2) a double, or (3) a FormulaError.  
    /// (By analogy, the value of an Excel cell is what is displayed in that cell's position
    /// in the grid.)
    /// 
    /// If a cell's contents is a string, its value is that string.
    /// 
    /// If a cell's contents is a double, its value is that double.
    /// 
    /// If a cell's contents is a Formula, its value is either a double or a FormulaError,
    /// as reported by the Evaluate method of the Formula class.  The value of a Formula,
    /// of course, can depend on the values of variables.  The value of a variable is the 
    /// value of the spreadsheet cell it names (if that cell's value is a double) or 
    /// is undefined (otherwise).
    /// 
    /// Spreadsheets are never allowed to contain a combination of Formulas that establish
    /// a circular dependency.  A circular dependency exists when a cell depends on itself.
    /// For example, suppose that A1 contains B1*2, B1 contains C1*2, and C1 contains A1*2.
    /// A1 depends on B1, which depends on C1, which depends on A1.  That's a circular
    /// dependency.
    /// </summary>

    public class Spreadsheet : AbstractSpreadsheet
    {
        // Private member variables for storing relationships
        private Dictionary<string, Cell> cellDatabase;
        private DependencyGraph depGraph;

        // Variable evaluator
        private Func<string, double> lookup;

        ///<summary>
        /// Spreadsheet zero argument constructor
        ///</summary>
        ///
        public Spreadsheet() : this(s => true, s => s, "default") { }

        /// <summary>
        /// Three argument constructor used to create a spreadsheet.
        /// </summary>
        /// <param name="isValid"></param>
        /// <param name="normalize"></param>
        /// <param name="version"></param>
        public Spreadsheet(Func<string, bool> isValid, Func<string, string> normalize, string version)
            : base(isValid, normalize, version)
        {
            // New Cell Database
            this.cellDatabase = new Dictionary<string, Cell>();

            // New Dependency relationship
            this.depGraph = new DependencyGraph();

            // Variable evaluator
            this.lookup = lookupMethod;

            // State variables
            this.Changed = false;
            this.Version = version;
        }

        /// <summary>
        /// Four argument constructor used to create a spreadsheet.
        /// </summary>
        /// <param name="isValid"></param>
        /// <param name="normalize"></param>
        /// <param name="version"></param>
        /// <param name="filename"></param>
        public Spreadsheet(String filename, Func<string, bool> isValid, Func<string, string> normalize, string version)
            : base(isValid, normalize, version)
        {
            // New Cell Database
            this.cellDatabase = new Dictionary<string, Cell>();

            // New Dependency relationship
            this.depGraph = new DependencyGraph();

            // Variable evaluator
            this.lookup = lookupMethod;

            // State variables
            this.Changed = false;
            this.Version = version;

            this.reconstructSpreadsheet(filename);
        }

        /// <summary>
        /// Reconstructs a given file and replicates it in the form of a spreadsheet.
        /// </summary>
        /// <param name="filename"></param>
        private void reconstructSpreadsheet(string filename)
        {
            if (Object.ReferenceEquals(filename, null))
            {
                throw new ArgumentNullException();
            }
            try
            {
                string name = "";
                string content = "";

                using (XmlReader reader = XmlReader.Create(filename))
                {
                    // Parse the file and display each of the nodes.
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (reader.IsStartElement() && reader.HasAttributes)
                                {
                                    if (!reader.GetAttribute("version").Equals(this.Version))
                                    {
                                        throw new SpreadsheetReadWriteException("Error while opening, writing or closing specified file: " + filename);
                                    }
                                    this.Version = reader.GetAttribute("version");
                                    break;
                                }
                                else if (reader.Name.Equals("name"))
                                {
                                    reader.Read();
                                    name = reader.Value;
                                    break;
                                }
                                else if (reader.Name.Equals("contents"))
                                {
                                    reader.Read();
                                    content = reader.Value;
                                    this.SetContentsOfCell(name, content);
                                    break;
                                }
                                else { break; }
                            default:
                                break;
                        }
                    }
                    reader.Close();
                    this.Changed = false;
                }
            }
            catch (Exception e)
            {
                throw new SpreadsheetReadWriteException("Error while opening, writing or closing specified file: " + filename);
            }
        }

        /// <summary>
        /// Lookup Method
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private double lookupMethod(string arg)
        {
            if (this.cellDatabase.ContainsKey(arg))
            {
                // Get current contents of cell
                Object obj = this.cellDatabase[arg].Contents;
                System.Type typ = obj.GetType();

                // String
                if (typ == typeof(String))
                {
                    throw new ArgumentException();
                }

                // Double
                else if (typ == typeof(Double))
                {
                    return (Double)this.cellDatabase[arg].Val;
                }

                // Formula
                else
                {
                    try
                    {
                        return (Double)this.cellDatabase[arg].Val;
                    }
                    catch (InvalidCastException invalid)
                    {
                        throw new ArgumentException();
                    }
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Enumerates the names of all the non-empty cells in the spreadsheet.
        /// </summary>
        public override IEnumerable<String> GetNamesOfAllNonemptyCells()
        {
            foreach (string name in this.cellDatabase.Keys)
            {
                yield return name;
            }

        }

        /// <summary>
        /// If name is null or invalid, throws an InvalidNameException.
        ///
        /// Otherwise, returns the contents (as opposed to the value) of the named cell.  The return
        /// value should be either a string, a double, or a Formula.
        /// </summary>
        public override object GetCellContents(String name)
        {
            if (Object.ReferenceEquals(name, null))
            {
                throw new InvalidNameException();
            }
            else
            {
                if (isValid(name) && this.IsValid(name))
                {
                    String normalizedCell = this.Normalize(name);

                    if (this.cellDatabase.ContainsKey(normalizedCell))
                    {
                        return cellDatabase[normalizedCell].Contents;
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    throw new InvalidNameException();
                }
            }

        }


        // MODIFIED PROTECTION FOR PS5
        /// <summary>
        /// If name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, the contents of the named cell becomes number.  The method returns a
        /// set consisting of name plus the names of all other cells whose value depends, 
        /// directly or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// In testing this will never reach full 100% of the code as name and content are checked before for ArgumentNullException and InvalidNameException
        /// </summary>
        protected override ISet<String> SetCellContents(String name, double number)
        {
            if (Object.ReferenceEquals(name, null))
            {
                throw new InvalidNameException();
            }
            else
            {
                if (isValid(name) && this.IsValid(name))
                {
                    String normalizedCell = this.Normalize(name);

                    if (this.cellDatabase.ContainsKey(normalizedCell))
                    {
                        this.cellDatabase[normalizedCell].Contents = number;
                        this.cellDatabase[normalizedCell].Val = number;
                        this.depGraph.ReplaceDependees(normalizedCell, new HashSet<string>());
                        HashSet<string> retContents = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                        recalculateCells(retContents);
                        this.Changed = true;
                        return retContents;

                    }
                    else
                    {
                        this.cellDatabase.Add(normalizedCell, new Cell(normalizedCell, number, number));
                        this.depGraph.ReplaceDependees(normalizedCell, new HashSet<string>());
                        HashSet<string> retContents = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                        recalculateCells(retContents);
                        this.Changed = true;
                        return retContents;

                    }
                }
                else
                {
                    throw new InvalidNameException();
                }
            }
        }

        // MODIFIED PROTECTION FOR PS5
        /// <summary>
        /// If text is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, the contents of the named cell becomes text.  The method returns a
        /// set consisting of name plus the names of all other cells whose value depends, 
        /// directly or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// In testing this will never reach full 100% of the code as name and content are checked before for ArgumentNullException and InvalidNameException
        /// </summary>
        protected override ISet<String> SetCellContents(String name, String text)
        {
            if (Object.ReferenceEquals(name, null))
            {
                throw new InvalidNameException();
            }
            else if (Object.ReferenceEquals(text, null))
            {
                throw new ArgumentNullException();
            }
            else
            {
                if (isValid(name) && this.IsValid(name) && !text.Equals(""))
                {
                    String normalizedCell = this.Normalize(name);

                    if (this.cellDatabase.ContainsKey(normalizedCell))
                    {
                        this.cellDatabase[normalizedCell].Contents = text;
                        this.cellDatabase[normalizedCell].Val = text;
                        this.depGraph.ReplaceDependees(normalizedCell, new HashSet<string>());
                        HashSet<string> retContents = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                        recalculateCells(retContents);
                        this.Changed = true;
                        return retContents;

                    }
                    else
                    {
                        this.cellDatabase.Add(normalizedCell, new Cell(normalizedCell, text, text));
                        this.depGraph.ReplaceDependees(normalizedCell, new HashSet<string>());
                        HashSet<string> retContents = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                        recalculateCells(retContents);
                        this.Changed = true;
                        return retContents;

                    }
                }
                else if (isValid(name) && this.IsValid(name) && text.Equals(""))
                {
                    String normalizedCell = this.Normalize(name);
                    HashSet<string> retContents = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                    this.depGraph.ReplaceDependees(normalizedCell, new HashSet<string>());
                    this.cellDatabase.Remove(normalizedCell);
                    recalculateCells(retContents);
                    this.Changed = true;
                    return retContents;
                }

                else
                {
                    throw new InvalidNameException();
                }
            }
        }

        // MODIFIED PROTECTION FOR PS5
        /// <summary>
        /// If formula parameter is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, if changing the contents of the named cell to be the formula would cause a 
        /// circular dependency, throws a CircularException.
        /// 
        /// Otherwise, the contents of the named cell becomes formula.  The method returns a
        /// Set consisting of name plus the names of all other cells whose value depends,
        /// directly or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// 
        /// In testing this will never reach full 100% of the code as name and content are checked before for ArgumentNullException and InvalidNameException
        /// </summary>
        protected override ISet<String> SetCellContents(String name, Formula formula)
        {

            // Previous contents
            object previousContents = null;

            // Previous dependees
            HashSet<String> dependees = new HashSet<string>(this.depGraph.GetDependees(name));

            if (Object.ReferenceEquals(name, null))
            {
                throw new InvalidNameException();
            }
            else if (Object.ReferenceEquals(formula, null))
            {
                throw new ArgumentNullException();
            }
            else
            {
                if (isValid(name) && this.IsValid(name))
                {
                    String normalizedCell = this.Normalize(name);

                    try
                    { 
                        if (this.cellDatabase.ContainsKey(normalizedCell))
                        {
                            previousContents = this.cellDatabase[normalizedCell].Contents;
                            this.cellDatabase[normalizedCell].Contents = formula;
                            this.cellDatabase[normalizedCell].Val = formula.Evaluate(this.lookup);
                            this.depGraph.ReplaceDependees(normalizedCell, formula.GetVariables());
                            HashSet<string> boolCircularDep = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                            recalculateCells(boolCircularDep);
                            return boolCircularDep;
                        }
                        else
                        {
                            this.cellDatabase.Add(normalizedCell, new Cell(normalizedCell, formula, formula.Evaluate(this.lookup)));
                            this.depGraph.ReplaceDependees(normalizedCell, formula.GetVariables());
                            HashSet<string> boolCircularDep = new HashSet<string>(GetCellsToRecalculate(normalizedCell));
                            recalculateCells(boolCircularDep);
                            return boolCircularDep;
                        }
                    }
                    catch (CircularException circle)
                    {
                        this.depGraph.ReplaceDependees(normalizedCell, dependees);
                        this.cellDatabase[name].Contents = previousContents;
                        throw circle;
                    }
                }
                else
                {
                    throw new InvalidNameException();
                }
            }
        }

        /// <summary>
        /// If name is null, throws an ArgumentNullException.
        ///
        /// Otherwise, if name isn't a valid cell name, throws an InvalidNameException.
        ///
        /// Otherwise, returns an enumeration, without duplicates, of the names of all cells whose
        /// values depend directly on the value of the named cell.  In other words, returns
        /// an enumeration, without duplicates, of the names of all cells that contain
        /// formulas containing name.
        ///
        /// For example, suppose that
        /// A1 contains 3
        /// B1 contains the formula A1 * A1
        /// C1 contains the formula B1 + A1
        /// D1 contains the formula B1 - C1
        /// The direct dependents of A1 are B1 and C1
        /// 
        /// In testing name will never be null or invalid or not in database as other preliminary methods prohibit this and throw errors before hand.
        /// </summary>
        protected override IEnumerable<String> GetDirectDependents(String name)
        {
            if (Object.ReferenceEquals(name, null))
            {
                throw new InvalidNameException();
            }
            else
            {
                if (isValid(name) && this.IsValid(name))
                {
                    String normalizedCell = this.Normalize(name);

                    if (this.cellDatabase.ContainsKey(normalizedCell))
                    {
                        return this.depGraph.GetDependents(normalizedCell);
                    }
                    else
                    {
                        return new HashSet<String>();
                    }
                }
                else
                {
                    throw new InvalidNameException();
                }
            }

        }

        // ADDED FOR PS5
        /// <summary>
        /// Returns the version information of the spreadsheet saved in the named file.
        /// If there are any problems opening, reading, or closing the file, the method
        /// should throw a SpreadsheetReadWriteException with an explanatory message.
        /// 
        /// In testing this will never reach the reader.Close as I have prohibited that in the code through saving a file.
        /// </summary>
        public override string GetSavedVersion(String filename)
        {
            if (Object.ReferenceEquals(filename, null))
            {
                throw new ArgumentNullException();
            }
            try
            {
                using (XmlReader reader = XmlReader.Create(filename))
                {
                    // Parse the file and display each of the nodes.
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (reader.IsStartElement() && reader.HasAttributes)
                                {
                                    return reader.GetAttribute("version");
                                }
                                else
                                {
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    reader.Close();
                    throw new SpreadsheetReadWriteException("Error while opening, writing or closing specified file: " + filename);
                }
            }
            catch (Exception e)
            {
                throw new SpreadsheetReadWriteException("Error while opening, writing or closing specified file: " + filename);
            }
        }

        // ADDED FOR PS5
        /// <summary>
        /// Writes the contents of this spreadsheet to the named file using an XML format.
        /// The XML elements should be structured as follows:
        /// 
        /// <spreadsheet version="version information goes here">
        /// 
        /// <cell>
        /// <name>
        /// cell name goes here
        /// </name>
        /// <contents>
        /// cell contents goes here
        /// </contents>    
        /// </cell>
        /// 
        /// </spreadsheet>
        /// 
        /// There should be one cell element for each non-empty cell in the spreadsheet.  
        /// If the cell contains a string, it should be written as the contents.  
        /// If the cell contains a double d, d.ToString() should be written as the contents.  
        /// If the cell contains a Formula f, f.ToString() with "=" prepended should be written as the contents.
        /// 
        /// If there are any problems opening, writing, or closing the file, the method should throw a
        /// SpreadsheetReadWriteException with an explanatory message.
        /// </summary>
        public override void Save(String filename)
        {
            if (Object.ReferenceEquals(filename, null))
            {
                throw new ArgumentNullException();
            }
            else
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = ("    ");
                try
                {
                    using (XmlWriter writer = XmlWriter.Create(filename, settings))
                    {
                        // Write start XML data.
                        writer.WriteStartElement("spreadsheet");
                        writer.WriteAttributeString("version", this.Version);

                        // Write cell/contents
                        foreach (String cellName in this.cellDatabase.Keys)
                        {
                            writer.WriteStartElement("cell");
                            writer.WriteElementString("name", cellName);

                            // Determine what to write for contents
                            Object obj = this.cellDatabase[cellName].Contents;
                            System.Type typ = obj.GetType();
                            if (typ == typeof(String))
                            {
                                writer.WriteElementString("contents", (String)obj);
                            }
                            else if (typ == typeof(Double))
                            {
                                writer.WriteElementString("contents", obj.ToString());
                            }
                            else
                            {
                                Formula content = (Formula)obj;
                                writer.WriteElementString("contents", "= " + content.ToString());
                            }

                            writer.WriteEndElement();
                            writer.Flush();
                        }

                        // Close
                        writer.WriteEndElement();
                        writer.Flush();
                        writer.Close();
                        this.Changed = false;

                    }
                }
                catch (Exception e)
                {
                    throw new SpreadsheetReadWriteException("Error while opening, writing or closing specified file: " + filename);
                }
            }

        }

        // ADDED FOR PS5
        /// <summary>
        /// If name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, returns the value (as opposed to the contents) of the named cell.  The return
        /// value should be either a string, a double, or a SpreadsheetUtilities.FormulaError.
        /// </summary>
        public override object GetCellValue(String name)
        {
            if (!Object.ReferenceEquals(name, null) && isValid(name) && this.IsValid(name))
            {
                String normalizedCell = this.Normalize(name);

                if (this.cellDatabase.ContainsKey(normalizedCell))
                {
                    return this.cellDatabase[normalizedCell].Val;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                throw new InvalidNameException();
            }
        }

        // ADDED FOR PS5
        /// <summary>
        /// If content is null, throws an ArgumentNullException.
        /// 
        /// Otherwise, if name is null or invalid, throws an InvalidNameException.
        /// 
        /// Otherwise, if content parses as a double, the contents of the named
        /// cell becomes that double.
        /// 
        /// Otherwise, if content begins with the character '=', an attempt is made
        /// to parse the remainder of content into a Formula f using the Formula
        /// constructor.  There are then three possibilities:
        /// 
        ///   (1) If the remainder of content cannot be parsed into a Formula, a 
        ///       SpreadsheetUtilities.FormulaFormatException is thrown.
        ///       
        ///   (2) Otherwise, if changing the contents of the named cell to be f
        ///       would cause a circular dependency, a CircularException is thrown.
        ///       
        ///   (3) Otherwise, the contents of the named cell becomes f.
        /// 
        /// Otherwise, the contents of the named cell becomes content.
        /// 
        /// If an exception is not thrown, the method returns a set consisting of
        /// name plus the names of all other cells whose value depends, directly
        /// or indirectly, on the named cell.
        /// 
        /// For example, if name is A1, B1 contains A1*2, and C1 contains B1+A1, the
        /// set {A1, B1, C1} is returned.
        /// </summary>
        public override ISet<String> SetContentsOfCell(String name, String content)
        {

            if (Object.ReferenceEquals(content, null))
            {
                throw new ArgumentNullException();
            }
            else if (Object.ReferenceEquals(name, null))
            {
                throw new InvalidNameException();
            }
            else if (isValid(name) && this.IsValid(name))
            {
                // Parse as double
                double dblparsed = 0;
                bool doubleParse = Double.TryParse(content, out dblparsed);
                if (doubleParse == true)
                {
                    return this.SetCellContents(name, dblparsed);
                }

                // Parse as formula
                string formulaNoSpaces = content.Replace(" ", "");
                if (formulaNoSpaces.Length > 0)
                {
                    bool formulaParse = formulaNoSpaces.ElementAt(0).Equals('=');
                    if (formulaParse == true)
                    {
                        string newFormula = content.Substring(content.IndexOf('=') + 1, content.Length - 1);
                        return this.SetCellContents(name, new Formula(newFormula, this.Normalize, this.IsValid));
                    }
                }

                // String
                return this.SetCellContents(name, content);
            }
            else
            {
                throw new InvalidNameException();
            }
        }

        // ADDED FOR PS5
        /// <summary>
        /// True if this spreadsheet has been modified since it was created or saved                  
        /// (whichever happened most recently); false otherwise.
        /// </summary>
        public override bool Changed { get; protected set; }

        /// <summary>
        /// Verify that the name of cell is valid
        /// </summary>
        /// <param name="name">Cell name</param>
        /// <returns>Boolean, whether name is valid or not</returns>
        private static bool isValid(String name)
        {
            // Patterns for individual tokens
            String varPattern = "(^[a-zA-Z]+[1-9][0-9]*$)";

            // Create regex to handle all cases of valid input
            Regex var = new Regex(varPattern);
            Match varMatch = null;

            // Watching for variables
            varMatch = Regex.Match(name, varPattern);

            // Verifying that variables are valid based on Normalizer and Validator delegates
            if (varMatch.Success)
            {
                return true;
            }
            else { return false; };
        }

        /// <summary>
        /// Recalculates the cells returned from getCellsToRecalculate().
        /// </summary>
        /// <param name="set">Cells returned from getCellsToRecalculate()</param>
        private void recalculateCells(IEnumerable<string> set)
        {
            foreach (string name in set)
            {
                if (this.cellDatabase.ContainsKey(name)) {
                System.Type type = this.cellDatabase[name].Contents.GetType();
                if (type == typeof(Formula))
                {
                    Formula recalculatedCell = (Formula)this.cellDatabase[name].Contents;
                    this.cellDatabase[name].Val = recalculatedCell.Evaluate(lookup);
                }
                }
            }
        }
    }
}