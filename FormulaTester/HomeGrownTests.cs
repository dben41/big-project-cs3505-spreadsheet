﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpreadsheetUtilities;
using System.Collections.Generic;
using System.Collections;

namespace FormulaTester
{
    [TestClass]
    public class UnitTest1
    {
        // Normalizer/Validator/Lookup Delegates
        public static Func<string, string> normalizer;
        public static Func<string, bool> validator;
        public static Func<string, double> lookup;

        // Methods that belong to delegates
        public static string normalMethod(string s)
        {
            return s + "_NORMALIZED";
        }

        public static bool isValidMethod(string s)
        {
            return s.Contains("_NORMALIZED");
        }

        public static double lookupVar(string s)
        {
            Dictionary<string, double> values = new Dictionary<string, double>();
            values.Add("a5_NORMALIZED", 5.5);
            values.Add("a6_NORMALIZED", 6.5);

            if (values.ContainsKey(s))
            {
                return values[s];
            }
            else
            {
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Designed to test simple constructor throwing no error, 1 parameter
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            Formula test1 = new Formula("1.0 + 2.0");
            Formula test2 = new Formula("1 + 2");
            Formula test3 = new Formula("1 + 2.0 * 3");
            Formula test4 = new Formula("1 * 2 / 2.1");
            Formula test5 = new Formula("1.0 - 2.0 * 1.9 / 3.5 + 4");
            Formula test6 = new Formula("x1 + 2.0 * 3");
            Formula test7 = new Formula("x1 * y2 / 2.1");
            Formula test8 = new Formula("x1 - y20 * z19 / 3.5 + 4");
        }

        /// <summary>
        /// Designed to test simple constructor with error prone formulas
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormulaFormatException))]
        public void TestMethod1a()
        {
            Formula test1a = new Formula("");
            Formula test2a = new Formula(null);
            Formula test3a = new Formula("=");
            Formula test4a = new Formula("~");
            Formula test5a = new Formula("@");
            Formula test6a = new Formula("2@...#21");
            Formula test7a = new Formula("(@...#21");
            Formula test8a = new Formula("3 + @...#21");
        }

        /// <summary>
        /// Designed to test the normalizer and validators
        /// </summary>
        [TestMethod]
        public void TestMethod2()
        {
            normalizer = normalMethod;
            validator = isValidMethod;
            Formula test1 = new Formula("1.0 + 2.0", normalizer, validator);
            Formula test2 = new Formula("1 + 2", normalizer, validator);
            Formula test3 = new Formula("1 + 2.0 * 3", normalizer, validator);
            Formula test4 = new Formula("1 * 2 / 2.1", normalizer, validator);
            Formula test5 = new Formula("1.0 - 2.0 * 1.9 / 3.5 + 4", normalizer, validator);
            Formula test6 = new Formula("x1 + 2.0 * 3", normalizer, validator);
            Formula test7 = new Formula("x1 * y2 / 2.1", normalizer, validator);
            Formula test8 = new Formula("x1 - y20 * z19 / 3.5 + 4", normalizer, validator);
        }

        /// <summary>
        /// Designed to test complex constructor with all 3 parameters, with errors
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormulaFormatException))]
        public void TestMethod2a()
        {
            normalizer = normalMethod;
            validator = isValidMethod;
            Formula test1 = new Formula("!_123asdf + 2.0 * 3", normalizer, validator);
            Formula test2 = new Formula("#123asdfdd * y2 / 2.1", normalizer, validator);
            Formula test3 = new Formula("?123asdfasdf - 123asdfssda * y332 / 3.5 + 4", normalizer, validator);
        }

        /// <summary>
        /// Designed to test the GetVariables method
        /// </summary>
        [TestMethod]
        public void TestMethod4()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula test1 = new Formula("asdf + 2.0 * 3", normalizer, validator);
            HashSet<string> result = new HashSet<string>(test1.GetVariables());
            Assert.IsTrue(result.Count == 1 && result.Contains("asdf_NORMALIZED"));

            Formula test2 = new Formula("asdfdd * y2 + y2 / 2.1", normalizer, validator);
            result = new HashSet<string>(test2.GetVariables());
            Assert.IsTrue(result.Count == 2 && result.Contains("asdfdd_NORMALIZED") && result.Contains("y2_NORMALIZED"));

            Formula test3 = new Formula("asdfasdf - asdfssda * asdfasdf - y332 / 3.5 + 4", normalizer, validator);
            result = new HashSet<string>(test3.GetVariables());
            Assert.IsTrue(result.Count == 3 && result.Contains("asdfasdf_NORMALIZED") && result.Contains("asdfssda_NORMALIZED") && result.Contains("y332_NORMALIZED"));
        }

        /// <summary>
        /// Designed to test the ToString method
        /// </summary>
        [TestMethod]
        public void TestMethod5()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula test1 = new Formula("asdf + 2.0 * 3", normalizer, validator);
            Assert.IsTrue(test1.ToString().Equals("asdf_NORMALIZED+2.0*3"));

            Formula test2 = new Formula("asdfdd * y2 + y2 / 2.1", normalizer, validator);
            Assert.IsTrue(test2.ToString().Equals("asdfdd_NORMALIZED*y2_NORMALIZED+y2_NORMALIZED/2.1"));

            Formula test3 = new Formula("asdfasdf - asdfssda * asdfasdf - y332 / 3.5 + 4", normalizer, validator);
            Assert.IsTrue(test3.ToString().Equals("asdfasdf_NORMALIZED-asdfssda_NORMALIZED*asdfasdf_NORMALIZED-y332_NORMALIZED/3.5+4"));

            Formula test1a = new Formula("asdf + 2.0 * 3");
            Assert.IsTrue(test1a.ToString().Equals("asdf+2.0*3"));

            Formula test2a = new Formula("asdfdd * y2 + y2 / 2.1");
            Assert.IsTrue(test2a.ToString().Equals("asdfdd*y2+y2/2.1"));

            Formula test3a = new Formula("asdfasdf - asdfssda * asdfasdf - y332 / 3.5 + 4");
            Assert.IsFalse(test3a.ToString().Equals("asdfasdf_NORMALIZED-asdfssda_NORMALIZED*asdfasdf_NORMALIZED-y332_NORMALIZED/3.5+4"));
        }

        /// <summary>
        /// Designed to test the Equals method
        /// </summary>
        [TestMethod]
        public void TestMethod6()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula tester = new Formula("x1+y2", normalizer, validator);
            Formula comparable = new Formula("x1  +  y2", normalizer, validator);
            Assert.IsTrue(tester.Equals(comparable));

            tester = new Formula("x1+y23", normalizer, validator);
            comparable = new Formula("x1  +  y23", normalizer, validator);
            Assert.IsTrue(tester.Equals(comparable));

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = new Formula("X1  +  Y2e", normalizer, validator);
            Assert.IsFalse(tester.Equals(comparable));

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = new Formula("X1  +  Y2asdfes", normalizer, validator);
            Assert.IsFalse(tester.Equals(comparable));
        }

        /// <summary>
        /// Designed to test the operator == method
        /// </summary>
        [TestMethod]
        public void TestMethod7()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula tester = new Formula("x1+y2", normalizer, validator);
            Formula comparable = new Formula("x1  +  y2", normalizer, validator);
            Assert.IsTrue(tester == comparable);

            tester = new Formula("x1+y23", normalizer, validator);
            comparable = new Formula("x1  +  y23", normalizer, validator);
            Assert.IsTrue(tester == comparable);

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = new Formula("X1  +  Y2e", normalizer, validator);
            Assert.IsFalse(tester == comparable);

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = null;
            Assert.IsFalse(tester == comparable);

            tester = null;
            comparable = null;
            Assert.IsTrue(tester == comparable);

            tester = null;
            comparable = new Formula("x1+y2", normalizer, validator);
            Assert.IsFalse(tester == comparable);
        }

        /// <summary>
        /// Designed to test the operator != method
        /// </summary>
        [TestMethod]
        public void TestMethod8()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula tester = new Formula("x1+y2", normalizer, validator);
            Formula comparable = new Formula("x1  +  y2", normalizer, validator);
            Assert.IsFalse(tester != comparable);

            tester = new Formula("x1+y23", normalizer, validator);
            comparable = new Formula("x1  +  y23", normalizer, validator);
            Assert.IsFalse(tester != comparable);

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = new Formula("X1  +  Y2e", normalizer, validator);
            Assert.IsTrue(tester != comparable);

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = null;
            Assert.IsTrue(tester != comparable);

            tester = null;
            comparable = null;
            Assert.IsFalse(tester != comparable);

            tester = null;
            comparable = new Formula("x1+y2", normalizer, validator);
            Assert.IsTrue(tester != comparable);
        }

        /// <summary>
        /// Designed to test the GetHashCode method
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void TestMethod9()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula tester = new Formula("x1+y2", normalizer, validator);
            Formula comparable = new Formula("x1  +  y2", normalizer, validator);
            Assert.IsTrue(tester.GetHashCode() == comparable.GetHashCode());

            tester = new Formula("x1+y23", normalizer, validator);
            comparable = new Formula("x1  +  y23", normalizer, validator);
            Assert.IsTrue(tester.GetHashCode() == comparable.GetHashCode());

            tester = new Formula("x1+y2", normalizer, validator);
            comparable = null;
            Assert.IsFalse(tester.GetHashCode() == comparable.GetHashCode());

            tester = null;
            comparable = null;
            Assert.IsTrue(tester.GetHashCode() == comparable.GetHashCode());

            tester = null;
            comparable = new Formula("x1+y2", normalizer, validator);
            Assert.IsTrue(tester != comparable);

        }

         /// <summary>
        /// Designed to test the areValidTokens method
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FormulaFormatException))]
        public void TestMethod11()
        {
            normalizer = normalMethod;
            validator = isValidMethod;

            Formula tester = new Formula("((x1+y2)+5", normalizer, validator);
            Formula comparable = new Formula(")(x1  +  y2", normalizer, validator);
            Formula comparable1 = new Formula(")(x1  +  y2)4", normalizer, validator);
        }
    }
}
